# Productividad filosófica de la moda: de Simmel a Lipovetsky

## __Resumen__

En discusiones recientes sobre filosofía de la moda, un asunto controversial ha sido si la moda se puede reducir a una expresión de la búsqueda del reconocimiento social, como sostiene Georg Simmel, o si hay algo más que sucede en la moda, leyéndola como un signo de espejos desde el cual se ve lo que constituye nuestro destino histórico más singular, tal como lo desarrolla Gilles Lipovetsky en su libro El imperio de lo efímero. El objetivo de este trabajo es valorar las ventajas de la segunda posición sobre la primera, sin dejar de reconocer el valor de cada una para comprender es imprescindible para comprender nuestra relación contemporánea con la sociedad, con nuestra cotidianidad y con nosotros mismos. De modo que la perspectiva de Lipovetsky presenta una nueva perspectiva para pensar la moda desde la filosofía, y viceversa. Lo cual posiciona a la moda como “piedra angular” de la sociedad, que ha llegado a la cima de su poder. 

__Palabras Clave:__ Filosofía, Moda, Filosofía de la moda, Simmel, Lipovetsky

__Key Words:__ Philosophy, Fashion, Fashion philosophy, Simmel, Lipovetsky

## Introducción: El valor filosófico de la moda

Si planteamos un ejercicio mental cuya  pregunta particular es que en el dado caso de despertar de un largo periodo en coma, ¿qué sería lo primero que pediríamos para ponernos al corriente con los hechos y sucesos en los que se encuentra el mundo? ¿qué responderíamos? Una revista de moda, dado que es el material que más nos ayudaría a ubicar el panorama global, y lo que nos permitiría tener un acercamiento al mundo. En otras palabras, el ejercicio mental nos postula una singular idea de cómo comprender nuestro contexto actual, que a su vez nos lleva a preguntarnos sobre la relevancia de la moda en nuestro marco actual. 
 
En _Fashion: A Philosophy_, Lars Svendsen habla de la moda como un fenómeno en cuya lógica nos encontramos inmersos: no sólo invade áreas de consumo, sino que permea en áreas del arte, política y ciencia.^1^ Svendsen habla de la moda como un fenómeno que se encuentra casi en el centro del mundo moderno,^2^ por lo que es pertiente su estudio para poder comprender nuestro contexto. 

>"La moda afecta la actitud de la mayoria de las personas hacia ellos mismos y hacia los demás,  aunque muchos lo negarían. La negación, sin embargo,normalmente se contradice por nuestros propios hábitos de consume – y tal es un fenómeno que que debería ser central en nuestros intentos de comprendernosa nostros mismos en nuestra situacion histórica."^3^ 

Es importante resaltar que para Svendsen, la moda _no es_ necesariamente la “clave universal” para comprender el mundo, y su postura es un punto de partida para comprender la conceptualización de la moda, y, al mismo tiempo, como un punto crítico de la moda, sin condenarla.^4^  El texto _Fashion: A Philosophy_ se enfoca en la relevancia de la moda con respecto de la identidad, enfocandose en el discurso de la moda.^5^  El punto desde el que retoma la discusión el filósofo noruego es desde la parte material de la moda, el vestido, la ropa,^6^  tal como lo menciona en sus propias palabras:

>“En términos generales, podemos distinguir entre dos principals categorías en nuestra comprensíon de lo que la moda es: uno puede afirmar que la moda refiere a la ropa, o que la moda corresponde a un mecanismo general, lógica o ideología que, entre otras cosas, se aplica al area de la ropa.^7^ 

Básicamente, Svendsen encuentra la materialidad de la moda como un punto focal en su conceptualización, pero también da cuenta de su productividad teórica que se enceuntra lindada a esta misma materialidad.

Ahora bien, una de las preguntas que surgen a partir de este ejercico mental es ¿porqué la moda ha tenido un “auge”, si es que lo podemos llamar de tal manera, en los últimos años, y que nos inlcina a pensar si esta relevancia de la moda es reciente o no, y la respuesta a esta última pregunta es que el interés filosófico y académico de la moda se extiende mucho tiempo atrás. 

De hecho, el mismo Svendsen hace dos entradas breves sobre los autores a los que referiremos un poco más adelante, Georg Simmel y Gilles Lipovetsky. El filósofo noruego hace una interesante alusión a ambos autores para poder ahondar en la discusión conceptual de la moda, para recalcar la dificultad que se presenta al intentar definir la moda de manera definitiva o singular.^8^

Por tanto, en la intepretación que Svendsen hace de la concepción que tiene el filósofo Georg Simmel de la moda, el autor de _Fashion: A Philosophy_, remite que Simmel concibe la moda como un _amplio fenómeno social_ que puede aplicarse a todas las áreas sociales, en las cuales la ropa es únicamnete una instancia entre muchas otras. En su interpetación de esta consideración que hace Simmel con respecto a la moda, Svendsen describe la moda como el “centro de interés” entre los distintos campos. En otras palabras, podemos entender la importancia de dedicar el estudio y el pensar la moda como herramienta crítica de nuestra contemporaneidad. Retomando lo que Svendsen nos dice, la moda nos brinda una clave de lectura hermeneútica, social, económica y política, entre muchos otros panoramas de la vida humana, que nos permite comoprender nuestro contexto contemporáneo de mejor manera. Lo cual, nos presenta un buen campo para hacer filosofía, para pensar las problemáticas que nos afligen y que nos cuestionan.

Por otra parte, Svendsen, al referirse a Gilles Lipovetsky habla de una independencia y particularidad de la moda, por lo que con la cita anterior de Svendsen, podemos conjetuar que la concepción de la moda tanto de Lipovetsky  y como de Simmel es la de un mecanismo social. De manera que, lo que parece distinguir a estos dos autores, según Svendsen, es su _teorización_ de la moda, pues la observan como un fenómeno que “afecta distintas esferas de la vida colectiva”.^9^ 

El hecho de que Svendsen remita a ambos autores, refuerza su explicación general de las dos concepciones que hay de la moda, una que remita a su materialidad, y la otra como un “mecanismo general, lógica o ideología”. De esta manera, queremos indicar que la que nos preocupa más en este trabajo es la segunda, sin embargo, no se busca descartar ninguna de las dos, y tal como lo hace Svendsen, queremos ilustar la postura desde dos autores: Simmel y Lipovetsky.

De esta manera nos es fructífero rescatar el pensamiento del sociólogo y filósofo alemán, Georg Simmel, para comprender la manera en que la moda puede llegar a comformar un campo de interés para el pensamiento filosófico. Y, a partir de esto, podemos preguntarnos sobre la importancia de pensar la moda, desde su trinchera, con respecto a los problemas actuales, explorando la descripción que hace Lipovetsky sobre el auge de la moda, y como esta “ha llegado a la cima de su poder”.^10^

Podríamos llegar a la apresurada conlusión de concordar con Lipovetsky, puesto que cada vez más la moda está al frente de la cultura popular, de nuestras conversaciones y de nuestras observaciones, tanto críticas como teóricas. También, podemos obrsevar como la moda refuerza su interes académico en la actividad económica que ha generado en la modernidad. De manera que con Lipovetsky podemos observar como es que en el campo de la moda podemos analizar distintos elementos de las sociedadades-democráticas-cosumistas como elemntos del poder que la moda ejerce sobre nuestra sociedad.^11^

A pesar de que la moda se ve inmersa en una economía capitalista, esta misma cuestiona nuevas maneras de pensar sus procesos de producción, su historia, y su futuro. Desde sus distintos objetos, como su publicidad, su fábricas y procesos de producción, el uso de sus prendas, la creatividad y repetición de sus siluetas, y entre muchos otros elementos más. Es a partir de sus diversos elementos que podemos repensar la relevancia de la moda en el pensamiento filosófico, lo que nos lleva al objetivo de este artículo, a fundamentar un valor filosófico de la moda, de debatir su productividad teórica en nuestro marco contemporáneo.

A esto, la principal tarea de este artñiculo podría articularse de la siguiente manera: ¿cómo es que la moda nos puede servir como objeto para establecer una lectura de nuestra sociedad? o, mejor dicho, ¿por qué es relevante destacar el análisis filosófico a partir de la moda en nuestro contexto contemporáneo?. Y, dentro de este cuestionamiento principal, comenzamos a preguntar ¿cuáles son las principales nociones o problemas que Simmel y Lipovetsky señalan a partir de la moda? y ¿por qué la moda?, ¿qúe es lo que nos puede aportar la moda en la investigación y en el análisis filosófico que la filosofía por sí sola no logra?
 
Así, buscamos hacer una comparación entre las dos posturas para analizar la moda como el pundo de partida para el pensamiento crítico filsófico. Por lo que proseguiremos a explorar cada una de las posturas para comprender los alcances filosóficos que ambos autores han hecho desde el campo de la moda. Al exponer las principales teorías sobre la moda de cada autor, Simmel y Lipovetsly, haremos una comparación de ambas posturas, destancando la que nos ayude a sotener mejor el carácter filosófico de la moda.

## __La moda según Georg Simmel__

Los autores Giovanni Matteuci y Stefano Marino, en el texto _Philosophical Perspectives of Fashion_, hablan de la crítica continua que recibe la moda por su carácter frívolo, y cómo es que, hasta recientemente, no se le atribuía valor intelectual alguno.^12^ Los autores señalan que la falta de un interés filosófico por la moda pueda ser por cómo se concibe a la naturaleza de la filosofía desde la tradición Occidental, principalmente enfocada a una actividad teórica que se concentra en la esencia de las cosas.^13^ Sin embargo, Mateucci y Marino notan que el paso de la moda al ámbito académico predomina en enfoques sociológicos, psicológicos, antropológicos o estudios culturales. 

>“En cualquier caso, es indudable que la moda representa una de las formas culturales que encarnan perfectamente las tendencias heterogéneas, multiformes, contradictorias, hasta cierto punto tal vez superficiales pero también emocionantes de la época actual. Desde este punto de vista, la moda puede asumirse como un espejo de la época contemporánea y parece entonces de importancia decisiva para obtener una visión de nosotros mismos y del mundo en que vivimos.”^14^

Ahora bien, dentro de este diagnóstico que hacen Mateucci y Marino sobre la moda como espejo de la época contemporánea, nos ayuda a enfatizar la relevancia de Simmel para este artículo, puesto que en el texto _Fashion_, el autor intenta encontrar los problemas que derivan en la moda misma, no porque le sean exclusivos, sino porque se observan de particular manera.

Georg Simmel fue un sociólogo y filósofo alemán de principios del siglo XX. Su teoría sobre la moda sigue manteniendo una relevancia, dado por su influencia en la discusión académica sobre el tema, tanto como por la relación y puntualización sobre el carácter de la moda como fenómeno social. La moda esta relacionada intrínsecamente con las formas sociales,^15^ de forma que la moda trasnforma constantemente distintos factores como el juicio estético, la ropa, el sistema de expresiones humanas, entre otros.^16^

En su exploración de fenómenos culturales, Simmel observa individuos que se reafirman a sí mismos a través de maneras únicas en la moda y en los estilos,, “por eso Simmel se sirve del fenómeno temporal de la moda para analizar los profundos cambios cambios culturales de la sociedad moderna.”^17^ De modo que, a través de su teorización de la moda, el filósofo y antropólogo alemán asevera que esta refiere a una diferenciación y equalizacion de clases, la cual podemos referir como la naturaleza dualística del hombre. Es decir, Simmel habla de la dualidad como un principio porquesubraya las oposiciones individual, y colectivo, diferencia e imitación.^18^ 

Para Simmel, hay una representación compleja de la identidad que se presenta en la moda, una especie de dualidad, o mejor dicho, una tensión que nos ayuda a comprender el provecho filosófico que proporciona el estudio de la moda.^19^ De mode que la relevancia del pensamiento filosófico de Simmel reside en la cotidianidad en la que se ve inmersa la moda, su cualidad sigilosa de entrelazarse en todos los aspectos sociales del ser humano.

Simmel es un autor que hace una construcción de la moda en directa relación con lo social, lo cual lo hace relevante para entender de mejor manera nuestro contexto actual, y los distintos factores que lo interpelan. Pues para el autor, “Pocos fenómenos de la vida social poseen una curva de conciencia tan puntiaguda como la moda.”^20^ En el texto _Fashion_, Simmel establece una interesante concepción del presente con respecto a la moda, y como es que esta última ocupa la línea divisioria entre el pasado y el futuro, que para el autor el presente es es una combinación de ambos.^21^

 También, podemos ver cómo el autor alemán comienza a adentrase a otros temas filosóficos que se puede equiparar a la dualidad que señalábamos anteriormente. De modo tal, los problemas que derivan de las “fuerzas” que Simmel atribuye a la moda, apuntan a problemas intrínsecos de la naturaleza humana y al presente particular donde se presentan.^22^ En primer lugar, en su texto _Fashion_, el autor dice lo siguiente:

>"Si estas fuerzas son expresadas filosóficamente en el contraste entre cosmoteismo y la doctrina de diferenciación inherente y la existencia separada de cada elemento cósmico, o si se basen en conflictos prácticos que representan el socialismo por un lado o el individualismo por el otro, siempre tendremos que lidiar con la misma forma fundamental de dualismo que se manifiesta biológicamente en le contraste entre hereditario y variación.”^23^

Desde una postura crítica de la moda, en la conceptualización filosófica que Georg Simmel presenta en su breve ensayo Fashion (1895) una de las características esenciales que nos dirigen hacia la concepción simmeleana de la moda es esta _dualidad_ a la que nos hemos estado refiriendo. Simmel describe que tal dualidad se presenta en una forma fundamental, la cual se manifiesta biológicamente en el _contraste_ entre lo hereditario y la variación.^24^ Es decir, para Simmel, las fuerzas antagonistas^25^ en la moda se ven expresadas también en las relaciones sociales y en la individualidad. Las dos tendencias son la imitación y la distinción, la de sentirnos parte de un grupo, pero al mismo tiempo buscar diferenciarnos.^26^ A todo esto, cuando el autor habla de la moda, no puede evitar hablar de esta dualidad, y cómo es que a su vez interpela al ser humano. El filósofo y sociólogo alemán añade: 

>"Este tipo de dualidad aplicada a nuestra naturaleza espiritual causa esta última a ser guiada por el esfuerzo hacia la generalización por un lado, y por el otro por el deseo de describir el elemento único y especial.”^27^

De este modo, de acuerdo con Simmel, esta dinámica es de donde la moda emerge. En esta _constante_ polarización de dos elementos opuestos es que la fuerza que impulsa a la moda surge, de este balance inestable que produce el cambio y el movimiento.^28^ Ahora bien, podemos decir que Simmel se concentra en la identificación y diferenciación social que se ven expresadas por medio de la mo-da, destacando que esta es producto de la distinción entre clases sociales.^29^

Por otro lado, en una reseña a Simmel, el filósofo Sergio Benvenuto plantea un susbstituto a la oposición distinción versus imitación que propone Simmel, por una más general: la oposición entre _intensidad y extensidad_.^30^ Para el filósofo italiano, la tensión de la moda que describe Simmel, no es estructuralmente distinta, sino lo que varía es su extensión.^31^ Desde esta nueva oposición, Benvenuto habla de la intensidad de las ideas o modas al estar concentrada en limitado grupo de personas, y la dismuinución de tal intensidad al volverse más extensa y popular. Lo cual podemos entender como dos manifestaciones de lo mismo, el cuestionamiento de este nuevo susbtituto yace en sí la opsoción de _distinción versus imitación_ de Simmel es la única forma por la que se inventan y crean rasgos culturales.^32^

De esta manera, podemos empezar a percibir que hay algo que se nos escapa, o que no termina de quedar muy claro. La moda, al pensarse exclusivamente en estos términos de dualidad social, del movimiento entre la diferenciación y la pertenencia de un individuo, no nos permite comprender el fenómeno de la moda de forma contemporánea. Pero existen todavía lagunas que contradicen estas mismas ideas, hasta el punto que podemos observar que la manera en que Simmel describe la moda no se ha presentado en la realidad contemporánea. Por ejemplo, Simmel habla de los cambios rápidos en la moda como resultado de un sistema de clases desarrollado, que requieren de un carácter cambiante en los estilos y prendas que se utilizan en un determinado grupo social, por lo que los grupos que no demuestran esta cualidad parecen quedar fuera de crear moda.

Así, dentro de los caracteres que ayudan a identificar y denominar algo como moda en la teoría de Simmel, se encuentra la cualidad transitoria, la cual el autor de _Fashion_ relaciona a la práctica de la diseminación de la siguiente manera: “En la práctica de la vida, cualquier otra cosa igualmente nueva y repentinamente diseminada no se llama moda, cuando estamos convencidos de su justificación material. Si, por otro lado, estamos seguros de que el hecho desaparecerá tan rápido como ocurrió, entonces lo llamamos moda.”^33^

De tal forma, este carácter trasitorio de la moda se ve alentado por la dualidad de la que nos hablaba al principio Simmel, de esa necesidad de permanencia por medio de la imitación y de diferenciación, que a su vez presenta un flujo más constante al estar presente los nervios de una época o grupo por diferenciarse. Sin embargo, es pertinente notar que también se podría expresar este nerviosimo de forma contratria, intentando no cambiar la moda, por el hecho de resaltar la distinción. Es decir, Simmel nos habla de la moda como un modelo que se basa en el cambio constante, en la diferenciación y los distintos grados, tal como nos lo explica Benvenuto con su oposición entre intensidad y extensidad. Pero justo esta resistencia a la moda, o al cambio constante también presenta una categoría dentro de la moda que puede llegar a influir a otros grupos sociales. Por ejemplo, los grupos africano-americanos en Estados Unidos, por mucho tiempo fueron rechazados, lo que se extendía a sus prendas y peinados particulares, y no eran considerados, hasta hace poco tiempo, “atractivos” o “a la moda”, pero en este grupo se mantuvieron estas costumbres en su ropa y en sus peinados, hasta que las casas de moda, y las clases sociales altas comenzaron a tomar inspiración y apropiarse de estos estilos.

Simmel da indicios de que hay algo de la moda que parece tener cierta vida propia, saliéndose del control de los individuos y de los grupos sociales, pues presenta sus propios motivos.^34^ Es algo que al “cultivarse” eleva la manera en que somos percibidos, pero es un movimiento que requiere de un refinamiento. Es decir, sus reglas y sus modas no son completamente medibles, por esto, Simmel señala que la ropa, las conductas sociales, las distracciones, son elementos que se atañen al estudio de la moda.^35^ En la reseña que hace Benvenuto a Fashion de Simmel, el filósofo italiano hace una interesante comparación entre la moda y la política, señalando el espacio del dualismo simmeleano entre imitación y distinción. Para Benvenuto la oscilación, el movimiento que genera los cambios en la moda y en la vida social, provee de una flexibilidad a modas, a una determinada multiplicidad. Es este espacio entre las fuerzas dualista que plantea Simmel, en dónde, según Benvenuto, se da el espacio a opiniones subversivas con la pretensión de dirigirse hacia una cohesión social,^36^ hacia la _unión_ de la que tanto nos habla Simmel.^37^  De tal manera, Benvenuto no son cambios radicales los que se producen, tanto en la política como en la moda, sino que son pasos graduales y pequeños como se da la oscilación entre las fuerzas, en donde la labor filosófica de ideas revolucionarias y modas extremas es lenta.^38^

En consideración a la ya expuesto, podemos ir observando como es que Simmel hace grandes señalamientos de cómo la moda estimula el pensamiento filosófico, y cómo es que se derivan los distintos elementos que conforman las concepciones de la vida humana.^39^ El filósofo alemán ahonda un poco más en las problemáticas que van más allá de la distinción social, pero no las explora a profundidad, sino que regresa siempre a la definición de las clases sociales. Simmel regresa a la mimesis que se presenta de las clases bajas hacia las clases altas, como un método de permanencia. Es decir, para Simmel, tanto el elemento de imitación como el de demarcación son importantes para la moda, expresado en las formas de propagación de la moda,^40^ y, de tal forma se ejerce la valoración de la moda ante la distinción y pertenencia con determinados grupos.

Pensar la moda de esta manera, podría decirse que es muy lineal, como si estos factores y comportamientos, que sin duda son sociales, fueran repetitivos, cíclicos y evidentes, pero podemos observar que no son del todo así. Tal como en la historia, en la ciencia, y en el arte hay patrones y conceptos que se repiten y vuelven a surgir en distintos momentos, la moda tiene elementos y fases que son retomadas por diferentes generaciones. Sin embargo, hay algo en la moda, respectivo a su materialidad, que modifica al cuerpo. Por lo tanto, la moda nos interpela de una forma muy peculiar, interceptando nuestros modelos de concepción del mundo. La moda se ve insertada en las estructuras de nuestro pensamiento y de nuestro lenguaje, en esos espacios de oscilación gradual, lo cual la hace un elemento significativo para poder comprender el contexto contemporáneo y sus problemas.

Para recapitular, desde el pensamiento de Simmel, en la moda se ve reflejada en una función doble, la cual remite a un movimiento presente en la vida humana y en sus esferas correspondientes, y como se desdoblan características y morales correspondientes a la clase de determinados grupos sociales.^41^ Herencia y variación, inclusión y exclusión, unión y segregación; la tensión de este vaivén es, según Simmel, la fuerza impulsora, e, inclusive, la condición de posibilidad de la moda. Además, para el filósofo y sociólogo alemán, el contraste de estas fuerzas es inseparable.^42^ De forma tal que Simmel establece un vínculo estrecho de la moda como un mero producto de las demandas sociales.^43^

### __La moda según Gilles Lipovetsky__

Por mucho tiempo, la moda no ha sido considerada un tema de interés filosófico. Así lo demuestra Gilles Lipovetsky en el texto _El imperio de lo efímero_, desde el cual plantea que dentro del campo intelectual sigue habiendo un rechazo a la moda, considerándola como un capricho ornamental del ser humano que no merece investigación científica, como cuestión superficial de una esfera inferior y ajena a la filosofía.^44^ Sin embargo, Lipovetsky cuestiona tal indiferencia ante la moda en la introducción de _El imperio de lo efímero_, señalando el lugar que esta ocupa en el contexto contemporáneo: 

>“La moda es celebrada en el museo y relegada al trastero de las preocupaciones intelectuales reales: está en todas partes, en la calle, en la industria y en los media, pero no ocupa ningún lugar en la interrogación teórica de las mentes pensantes.”^45^

Gilles Lipovetsky nació en París en el año 1944, y, es filósofo por la Sorbona de Grenoble. Su principal tema de enfoque es la _hipermodernidad_ y se ha destacado por el análisis de la posmodernidad al que se ha dedicado en sus diversos ensayos. En el texto, _El imperio de lo efímero_ (1987), el filósofo y sociólogo Gilles Lipovetsky presenta el caso sobre la pertinencia e importancia de la moda, más allá del escaso interés teórico que despierta en una compresión global del fenómeno.^46^ El filósofo francés reconoce la infinita complejidad de la moda como fenómeno contemporáneo, y, a su parecer, la versatilidad de esta se ve truncada al propagarse indiferencia dentro del campo de las preocupaciones intelectuales, al ser excluida de los intentos en explicar el funcionamiento de las sociedades modernas.^47^

En la introducción de su libro, Lipovetsky esboza un claro panorama frente a la concepción del fenómeno de la moda, y cómo este se ve limitado por una interpretación global que comprende  a la moda a partir de una _regulación_, y que adopta un credo común: “la versatilidad encuentra su lugar y su verdad última en la existencia de las rivalidades de clase […]”.^48^ Y, en consecuencia, esta concepción global devela una _crisis profunda_ al no originar dimensiones teóricas.^49^ Para el autor de _El imperio de lo efímero_, se puede observar en la moda una situación sociohistórica particular que caracteriza al Occidente y a la misma modernidad.^50^ Tal crisis se encuentra denominada por la problemática que envuelve al fenómeno de la moda, expuesto en palabras del autor de la siguiente manera:

>“La moda se ha convertido en un problema vacío de pasiones y de compromisos teóricos, en un pseudo-problema cuyas respuestas y razones son conocidas de antemano; el caprichoso reino de la fantasía no ha conseguido provocar más que la pobreza y la monotonía del concepto.”^51^

De esta manera, nos podemos preguntar ¿de que manera nos podemos separar de la com-prensión global sobre la moda? Y, ¿en donde reside la relevancia de la moda en nuestro contexto contemporáneo? Por tanto, Lipovetsky nos sirve para comprender la clave de lectura contemporánea de la moda, la cual está basada, tal como lo dice la cita anterior, en una sesgada y normada pasividad que ha entorpecido el análisis crítico de su versatilidad.^52^ Y, tal versatilidad es lo que Lipovetsky relaciona con el _enigma de la moda_, es decir, a su posibilidad de comprenderse como espejo de lo que constituye nuestro destino histórico, con el fin de sacudir ese objeto fútil y contradictorio que por excelencia la constituye, y, que, por lo mismo, “debería de estimular razón teórica”.^53^ 

Es decir, el autor de El _imperio de lo efímero_ posee la destreza de localizar en la invariable reducción de la explicación de las manifestaciones y lógica de la moda, los fenómenos de estratificación social que han ocasionado la falta del interés en la aproximación conceptual y científica que pueda suscitar el fenómeno de la moda.^54^ A su vez, el filósofo francés reconoce que no hay una falta de material sobre el tema, sino que la falta de interrogación es lo que diluye su complejidad, su carácter contradictorio y su opacidad.^55^ Por lo tanto, tal como Lipovetsky describe el sesgo en la comprensión global del fenómeno de la moda, la clave que no permite salir del esquema de la distinción social se puede identificar,^56^ hasta cierto punto, en la teoría simmeleana. Por ejemplo, retomando el vínculo estrecho de la moda como un mero producto de las demandas sociales al que refiere Simmel en su análisis teórico de la moda, comprendiendo la moda de la transitoriedad en determinados estratos sociales.^57^ Por tanto, en el intento de redefinir la concepción de la moda, Lipovestky esclarece lo siguiente:

>“El esquema de la distinción social, que se impone como la clave soberana de la inteligibiliad de la moda, tanto en la esfera del vestido como en la de los objetos y la cultura moderna, es fundamentalmente incapaz de explicar lo más significativo: la lógica de la inconstancia, las grandes mutaciones organizativas y estéticas de la moda. Esta es la idea que está en la base de la reinterpretación global que proponemos.”^58^

Como efecto de esta regulada comprensión del fenómeno de la moda, el filósofo y sociólogo francés propone un _re-significación_ en la manera en que entendemos el fenómeno de la moda, considerándolo como “[…] una institución excepcional, altamente problemática”.^59^ A partir de la perspectiva de Lipovetsky, este artículo busca explorar la perspectiva postiva sobre la moda y sus  posibilidades teóricas, las cuales nos pueden servir para remitir al trabajo del filósofo y sociólogo alemán, Georg Simmel, para lograr comprender la contribución filosófica que se hace por medio de la moda, no solo en la segunda mitad del siglo XIX y XX, sino como nos brinda un modelo proporcional a la forma en que narramos la contemporaneidad.

En otras palabras, el panorama contemporáneo, según Lipovetsky, “[…] es uno de los espejos donde se ve lo que constituye nuestro destino histórico más singular: la negación del poder inmemorial del pasado tradicional, la fiebre moderna de las novedades y la celebración del presente social”^60^, tal como lo decían Mateucci y Marino en _Philosophical Perspectives on Fashion_, estabelcen que la moda es una de las formas culturales que encarna, de manera perfecta, las tendencias de la actualidad,^61^ y, para Simmel, la historia de la sociedades esta reflejada en los conflictos y compromisos, entre la adaptación social y la desviación individual de sus demandas, que remiten a las fuerzas antagónicas que discute en _Fashion_.^62^ En consecuencia, la conjetura de estas tres posturas con respecto a la moda, nos permiten analizar y exponer la particular apertura crítica de la moda.

Por lo tanto, la tarea que aquí se pretende es la de desempolvar las concepciones teóricas que se tienen sobre el fenómeno de la moda,^63^ de similar manera a como lo describe Lipovetsky, la elaboración filosófica de la moda se ve truncada con la regulada posibilidad en el campo de su discusión. Es por eso que el autor francés habla de _dinamizar la interrogación_ sobre la moda,^64^ por lo que el intento está en _sacudir_ las problemáticas ya expuestas, específicamente en las teorías de Simmel y Lipovetsky, para así poder entender la moda más allá de la expresión del estatus social con respecto a la búsqueda de prestigio y validación dentro de una comunidad, por medio de manifestaciones materiales llamativas. De tal manera, Lipovetsky nos sirve para localizar la moda en la contemporaneidad, e, identificar el papel que ocupa en las sociedades modernas. En contraste a Simmel, Lipovetsky busca evidenciar lo que ha generado la concepción global negativa de la moda, describiendo el poder en la contemporaneidad. Y, en las palabras del filósofo francés planteado del siguiente modo:

>“Explosión de la moda: ya no tiene epicentro, ha dejado de ser el privilegio de una elite social, todas las clases son arrastradas por la ebriedad del cambio y las fiebres del momento; tanto la infraestructura como la superestructura se han sometido, si bien en diverso grado, al reino de la moda.”^65^

Lipovetsky nos habla de la moda plena como época en la que ha ampliado su fuerza en ámbitos de la vida colectiva,^66^ en otras palabras, Lipovetsky habla de la moda como una “forma general que actúa en el todo lo social”, ^67^ y no como un sector específico y periférico. Por lo tanto, podemos ver con como es que “nos hallamos inmersos en la moda”,^68^ lo cual conlleva a que el autor de _El imperio de lo efímero_ establezca una “triple operación” que define a la moda: lo _efímero_, la _seducción_ y la _diferenciación marginal_.^69^

Por consiguiente, el filósofo francés señala con este nuevo dominio de la moda, característi-ca de la modernidad en Occidente, el surgimiento de un nuevo tipo de sociedad que se ve guiada por las imágenes y la sociabilidad por selección.^70^ Por lo tanto, podemos comenzar a entender el interés de Lipovetsky por renovar la comprensión global de la moda, que, según el filósofo francés, limita el origen de concepciones teóricas y desanima la aproximación conceptual. Así, la renovación de la comprensión de la moda podríamos equipararla a la comprensión de la manera en que las sociedades contemporáneas se han replanteado. Es decir, el filósofo francés busca repensar estas sociedades, no desde la decadencia ni el vacío de ideales superiores,^71^ sino como una nueva aproximación ante tales ideales, desde la reconstrucción y la transformación.

Sin embargo, a pesar de que hay un notable poder de la moda en los ámbitos sociales, Lipovetsky advierte que “es evidente que muchos aspectos esenciales de la vida colectiva tienen poco que ver con la moda […]”.^72^ La moda _cohabita_ con otros campos, como el de la economía y la seguridad ciudadana, por lo que el filósofo francés reafirma que la moda _no es la única luz_ con la se puede analizar a la sociedad contemporánea. Por tanto, la fuerza de la _forma moda_ es que puede combinar los distintos elementos de la sociedad sin absorberlos por completo en su lógica.^73^

Ahora bien, el filósofo noruego, Lars Svendsen, también nota la falta de atención a la moda en el ámbito filosófico, al igual que menciona que hay excepciones como Kant, Walter Benjamin, o Theodor W. Adorno, que dedican cierta atención al tema. Pero Svendsen denota a dos filósofos que han dedicado libros enteros al tema de la moda, Georg Simmel y Gilles Lipovetsky.^74^ Para Svendsen, la importancia de las investigaciones filosóficas sobre moda reside en su significado, analizando desde esta perspectiva una pieza de literatura que básicamente ridiculiza a la moda.^75^ Pero, lo que el filósofo noruego reconoce en este texto de literatura, es como el autor da cuenta de la crucial importancia de la ropa para la constitución del ser humano.^76^ A todo esto, Svendsen busca destacar una relación entre moda e identidad, dejando de referirse únicamente a la diferenciación de clase, formando parte de lo individual.^77^ Svendsen resalta el estímulo y aburrimiento de vivir a partir de la moda como principio, desde el cual “nos libramos de una serie de conexiones tradicionales, pero nos volvemos esclavos de nuevas instituciones”.^78^

De manera similar, Lipovetsky habla de la moda en la contemporaneidad como el “apogeo de su imperio”^79^, lugar desde donde “[…] la seducción y lo efímero han llegado a convevertirse en los principios organizativos de la vida colectiva moderna”.^80^ De manera similar, Svendsen, en _Fashion: A Philosophy_, reconoce la dificultad de mantenerse fuera de la esfera de la moda.^81^ Al igual que Svendsen, Lipovetsky no deja de reconcer tales denuncias que se le hacen a la moda, y como es que puede observarse en paralelo a carcaterísticas alienantes y tiránicas en su hegemónia,^82^ como la institucionalización del consumo o el hipercontrol de la vida cotidiana, sistemas fetichistas y perversos que perpetua la dominación de clase, la dominación burocrático-capitalista, etc.^83^ Pero, para el filósofo francés, este enfoque deja de lado una parte fundamental del reino de la moda, la demanda que hace ante la razón y la autonomía.^84^ Lipovetsky se encarga de no ceder al mensaje de aniquilación, de declive y hundimiento de la cultura, como productos de los efectos perversos de la moda, sino que parte desde su concepción como _agente_ por exelencia de la espiral individualista y de la consolidación de las nuevas sociedades liberales. A pesar de que resalta el poder _positivo_ de la moda, Lipovetsky no descarta las inquietudes y las denuncias, sino que las reconoce, sin limitar las capacidades de las “democracias frívolas” para afrontarse al futuro.^85^

## __Conclusión__

Para fines de este artículo, la moda la entendemos como un elemento que ayuda a aterrizar y relacionar la filosofía con nuestra actualidad, con las complejidades que este fenómeno presenta en la sociedad contemporánea, tal como lo expone Lipovetsky en _El imperio de lo efímero_. Con las perspectivas teoricas de Simmel y Lipovetsky, podemos comenzar a dilucidar como es que el fenómeno de la moda dota a la filosofía del peso de la actualidad. La relevancia filosófica de la moda es su lenguaje visual, como modelo dominante en las sociedades modernas, y lo que logra comunicar el contexto que nos envuelve, que nos define, qué nos configura.png)

El carácter filosófico de la moda, siguiendo el pensamiento de Simmel y de Lipovetsky, entre otros, parece ser que se da en el encuentro, no solo en la vestimenta o en la temporalidad, o en el individuo, sino en como los factores configuran un espacio y un tiempo determinado donde se expresa una identidad grupal o individual. Es la intervención material que va más allá. Es decir, que atraviesa múltiples esferas desde las que se piensa el ser humano (economía, filosofía, cultural, política, etc.) El valor filosófico de la moda está en esta misma transitoriedad de la que nos habla Simmel, de ese incesante cambio criticado por Svendsen como una pluralidad estilística que deja de ser significativa.^86^ Pero esa transitoriedad, tal como lo habiamos dicho anteirormente, entre esas fuerzas duales de la moda, es donde se abren los espacios a la multiplicidad, en este caso, a la multiplicidad de teorias y posiciones que permiten establecer una discusión subtancial sobre los lacances filosóficos de la moda.

El valor tórico de la moda, no reside con un autor espcifico, pero la postura de Lipovetsky es la que nos puede ayudar a mantener la atención en esos epsacios de apertura, ya sea al sacudir las concepciones esatblecidadas que limitan la problematizacion y el analisis de la complejidad del fenómeno u objeto en cuestión. 

Dentro de los problemas de nuestro contexto, parece que la moda es un asunto fútil en comparación a los crímenes de violencia, a la pobreza extrema, a las catástrofes producto del calentamiento global, a la migración, y a todos los demás problemas que hay en el mundo. Epecialmente, en épocas de pandemias y crisis económicas, parece difícil encontrar el sentido para leer o escribir sobre la moda, pero creo que hay algo de la moda que es preciso rescatar, tal como lo decía Simmel, se ve intrínsecamente relacionada con las formas sociales.^87^ La moda esta relacionada intrínsecamente con las formas sociales,^88^ que nos recuerda que se da en esta sociabilidad. Es claro que no de la misma manera en todos los lugares o niveles sociales, porque, a pesar de que la moda no se puede agotar la distinción social, esta última sigue siendo parte de la esfera de la moda. De forma que la moda si tiene una importancia en nuestro contexto, aún al no ser la más urgente, plantea su insitencia en su cotidianeidad enigmática. Es decir, Lipovetsky lo descirbe de manera más elocuente: “El misterio de la moda está ahí, en la unicidad del fenómeno, en la emergencia e institución de su reino en Occidente moderno y ninguna otra parte.”^89^ 

De nuevo, la moda es un fenómeno por el cual nos situamos en nuestro presente, es lo que delimita los campos de la vida cotidiana, y marca la pauta de las relaciones sociales. Sí, podemos cuestionar la limitada libertad de elección de la moda, tal como lo hace Sevndsen a Lipovetsky, pues, para el filósofo noruego no constituye una verdadera diferencia.^90^ Es esta una de las formas en que la moda, a mi parecer, ha logrado infiltrarse en las diversas esferas de la vida humana, puesto que de entrada no impone una unívoca lógica que somete a todo lo que se encuentra en su camino. Más bien, la moda parece adaptarse a modelos y estructuras predominantes que la han mantenido pulsante en la mdoernidad, pero también, ha generado contrastes a estas mismas normatividades por su carácter transitorio, que, desde Simmel, la constituye.

Asimismo, podríamos plantear la pregunta sobre si ¿la moda se puede comprender como un sistema que nos encadena o que nos libera? Dentro de la misma cosntitución de la moda, es una cuestión de contemplar las tiranías y las agencias que produce, desde Svendsen, podríamos contemplar el efecto contrario a la formacion de identidad, tal como lo expone el filósofo noruego:

>“Podemos afirmar con seguridad que la moda funciona relativamente mal como guía para la vida. Lo que puede ofrecer, a pesar de todo, no agrega mucho a nuestras vidas en la forma de significado esencial, y cuando la lógica de la moda se convierte en la norma para la formación de identidad, tiene el efecto contrario: disuelve la identidad.”^91^ 

Por otro lado, podemos retomar que esta objeción se da dentro del mismo fenómeno de la moda, es decir, sus críticas son parte del carácter transitorio que expone Simmel, y que desde Lipovetsky se puede entender como “[…]el agente supremo de la dinámica indidividualista en sus diversas manifestaciones”,^92 es decir, para el filósofo francés, la moda permite que se extienda la controversia la controversia pública.^93^ Simmel nos habla de esta variabilidad y mutabilidad de la moda, que atraviesa al ser humano, esta a los márgenes de su constitución y en su autoconsideración, que Simmel describe de manera muy perspicaz:

>“La moda siempre se considera algo externo, incluso en ámbitos fuera de los simples estilos de vestimenta, ya que la forma de mutabilidad en la que se presenta al individuo es, en todas las circunstancias, un contraste con la estabilidad del sentimiento del ego. De hecho, este último, a través de este contraste, debe tomar conciencia de su duración relativa. La variabilidad de esos contenidos puede expresarse como mutabilidad y desarrollar su atracción solo a través de este elemento duradero. Pero por esta misma razón, la moda siempre se encuentra, como he señalado, en la periferia de la personalidad, que se considera a sí misma como una _pièce de résistance_ para la moda, o al menos puede hacerlo cuando se le pide".^94^

La moda podemos comenzar a verla como espacio, como un lugar desde el cual la contradicción simmelea toma el matiz de la discusión problemática de Lipovetsky. Como el lugar que permite la constitución de contradicciones, lo cual mantiene andado su fuerza y su movilidad. Son esas mismas identidades que cosntituye de manera tan intengrada, que pueden resistirse a la misma. La moda, desde su materialidad nos comienza a desdoblar los márgenes en que se juega la identidad individual, y las formas de sociabilidad colectiva en un contexto determinado, la contemporaneidad. 

## __Referencias al pie de página__

^1^ Lars Svendsen, _Fashion: A Philosophy_, trad. John Irons (Londres: Reaktion Books, 2006), 10.

^2^ Svendsen, _Fashion: A Philosophy_, 10.

^3^ Svendsen, _Fashion: A Philosophy_, 10. “Fashion affects the attitude of most people towards both themselves and others, though many would deny it. The denial, however, is normally contradicted by our own consumption habits — and such it is a phenomenon that ought to be central to our attempts to understand ourselves in our historical situation.”

^4^ Svendsen, _Fashion: A Philosophy_, 11.

^5^ Svendsen, _Fashion: A Philosophy_, 11.

^6^ Svendsen, _Fashion: A Philosophy_, 11.

^7^ Svendsen, _Fashion: A Philosophy_, 12.“Generally speaking, we can distinguish between two main categories in our understanding of what fashion is: one can either clam that fashion refers to clothing, or that fashion is a general mechanism, logic or ideology that, among other things, applies to the area of clothing.”

^8^ Svendsen, _Fashion: A Philosophy_, 12.

^9^ Svendsen, _Fashion: A Philosophy_, 13.

^10^ Gilles Lipovetsky, _El imperio de lo efímero_, trad. Felipe Hernández y Carmen López (Barcelona: Editorial Anagrama, 1996), 13.

^11^ Lipovetsky, _El imperio de lo efímero_, 12.

^12^ Giovanni Matteucci y Stefano Marino eds., _Philosophical Perspectives On Fashion_ (Londres y Nueva York: Bloomsbury Academic, 2017), 1.

^13^ Matteucci y Marino, _Philosophical Perspectives On Fashion_, 2.

^14^ Matteucci y Marino, _Philosophical Perspectives On Fahsion_, 1. “In any case, it is out of doubt that fashion represents one of the cultural forms that perfectly embody the heterogeneous, multiform, contradictory, to some extent perhaps superficial but also exciting tendencies of the present age. From this point of view, fashion can be assumed as a mirror of the contemporary age and appears then of decisive importance in order to gain insight into ourselves and the world we live in.”

^15^ Simmel, “Fashion”, 544.

^16^ Simmel, “Fashion”, 545.

^17^ De Simone, Liliana. “Georg Simmel y la Moda: hacia una comprensión de la sociedad de consumo en la ciudad” En _Ciudades de George Simmel_, Francisca Márquez, ed. (Santiago: Ediciones Universidad Alberto Hurtado, 2012), 2.

^18^ Jorge Lozano, “Simmel: La Moda, el atractivo formal del límite”, _REIS, Revista Española de Investigaciones Sociológicas_, núm. 89, (enero-marzo 2000): 239.

^19^ Simmel, “Fashion”, 541.

^20^ Simmel, “Fashion”, 547. “Few phenomena of social life possess such a pointed curve of consciousness as does fashion.”

^21^ Simmel, “Fashion”, 547.

^22^ Simmel, “Fashion”, 543.

^23^ Simmel, “Fashion”, 542.“Whether these forces be expressed philosophically in the contrast between cosmotheism and the doctrine of inherent differentiation and separate existence of every cosmic element, or whether they be ground in practical conflict representing socialism on the one hand or the individualism on the other, we have always to deal with the same fundamental form of duality which is manifested biologically in the contrast between heredity and variation.”

^24^ Simmel, “Fashion”, 542.

^25^ Simmel, “Fashion”, 541.

^26^ Simmel, “Fashion”, 542.

^27^ Simmel, “Fashion”, 542.“This type of duality applied to our spiritual nature causes the latter to be guided nature causes the latter to be guid-ed by the striving towards generalization on the one hand, and on the other by the desire to describe the single, special element.” 

^28^ Sergio Benvenuto, “Book Review of Georg Simmel’s Fashion”,_Journal of Artificial Societies and Social Simulation_, Vol. 3, núm. 2 (Marzo 2000): 1.

^29^ Simmel, “Fashion”, 544.

^30^ Benvenuto, “Book Review of Georg Simmel’s _Fashion_”, 5.

^31^ Benvenuto, “Book Review of Georg Simmel’s _Fashion_”, 4.

^32^ Benvenuto, “Book Review of Georg Simmel’s _Fashion_”, 4.

^33^ Simmel, “Fashion”, 548. “In the practice of life anything else similarly new and suddenly dissemi-nated is not called fashion, when we are convinced of its material justification. If, on the other hand, we feel certain that the fact will vanish as rapidly as it came, then we call it fashion.”

^34^ Simmel, “Fashion”, 544. 

^35^ Simmel, “Fashion”, 544.

^36^ Benvenuto, “Book Review of Georg Simmel’s _Fashion_”, 9.

^37^ Simmel, “Fashion”, 546.

^38^ Benvenuto, “Book Review of Georg Simmel’s _Fashion_”, 9.

^39^ Simmel, “Fashion”, (Mayo, 1957): 545.

^40^ Simmel, “Fashion”, (Mayo, 1957): 544.

^41^ Simmel, “Fashion”, (Mayo, 1957): 544.

^42^ Simmel, “Fashion”, 544.

^43^ Simmel, “Fashion”, 544.

^44^ Lipovetsky, _El imperio de lo efímero_, 9.

^45^ Lipovetsky, _El imperio de lo efímero_, 9.

^46^ Lipovetsky, _El imperio de lo efímero_, 10.

^47^ Lipovetsky, _El imperio de lo efímero_, 9.

^48^ Lipovetsky, _El imperio de lo efímero_, 10.

^49^ Lipovetsky, _El imperio de lo efímero_, 10.

^50^ Lipovetsky, _El imperio de lo efímero_, 11.

^51^ Lipovetsky, _El imperio de lo efímero_, 10.

^52^ Lipovetsky, _El imperio de lo efímero_, 11. 

^53^ Lipovetsky, _El imperio de lo efímero_, 10. 

^54^ Lipovetsky, _El imperio de lo efímero_, 10. 

^55^ Lipovetsky, _El imperio de lo efímero_, 10.

^56^ Lipovetsky, _El imperio de lo efímero_, 10. 

^57^  Simmel, "Fashion", 544-548.

^58^ Lipovetsky, _El imperio de lo efímero_, 11.

^59^ Lipovetsky, _El imperio de lo efímero_, 11.

^60^ Lipovetsky, _El imperio de lo efímero_, 11.

^61^ Matteucci y Marino, _Philosophical Perspectives on Fashion_, 1.

^62^ Simmel, "Fashion", 542.

^63^ Lipovetsky, _El imperio de lo efímero_, 10.

^64^ Lipovetsky, _El imperio de lo efímero_, 10.

^65^ Lipovetsky, _El imperio de lo efímero_, 175.

^66^ Lipovetsky, _El imperio de lo efímero_, 175.

^67^ Lipovetsky, _El imperio de lo efímero_, 175.

^68^ Lipovetsky, _El imperio de lo efímero_, 175.

^69^ Lipovetsky, _El imperio de lo efímero_, 175.

^70^ Lipovetsky, _El imperio de lo efímero_, 175.

^71^ Lipovetsky, _El imperio de lo efímero_, 176.

^72^ Lipovetsky, _El imperio de lo efímero_, 176.

^73^ Lipovetsky, _El imperio de lo efímero_, 177.

^74^ Svendsen, _Fashion: A Philosophy_, 17.
 
^75^ Svendsen, _Fashion: A Philosophy_, 18.

^76^ Svendsen, _Fashion: A Philosophy_, 19.

^77^ Svendsen, _Fashion: A Philosophy_, 19.

^78^ Svendsen, _Fashion: A Philosophy_, 19.

^79^ Lipovetsky, _El imperio de lo efímero_, 13.

^80^ Lipovetsky, _El imperio de lo efímero_, 13.

^81^ Svendsen, _Fashion: A Philosophy_, 19.

^82^ Svendsen, _Fashion: A Philosophy_, 156.

^83^ Lipovetsky, _El imperio de lo efímero_, 177-178.

^84^ Lipovetsky, _El imperio de lo efímero_, 178.

^85^ Lipovetsky, _El imperio de lo efímero_, 14.

^86^ Svendsen, _Fashion: A Philosophy_, 156.

^87^  Simmel, “Fashion”, 544.

^88^  Simmel, “Fashion”, 544.

^89^ Lipovetsky, _El imperio de lo efímero_, 24.

^90^ Svendsen, _Fashion: A Philosophy_, 156.

^91^ Svendsen, _Fashion: A Philosophy_, 157. “We can safely state that fashion functions relatively badly as a guide to life. What it can offer does not, despite everything, add all that much to our lives in the way of essential significance, and when the logic of fashion becomes the norm for the formation of identity, it has the opposite effect – it resolves identity.”

^92^ Lipovetsky, El imperio de lo efímero, 17.

^93^ Lipovetsky, El imperio de lo efímero, 17.

^94^ Simmel, “Fashion”, 552. “Fashion always continues to be regarded as something external, even in spheres outside of mere styles of apparel, for the form of mutability in which it is presented to the individual is under all circumstances a contrast to the stability of the ego-feeling. Indeed, the latter, through this contrast, must become conscious of its relative duration. The changeableness of those contents can express itself as mutability and develop its attraction only through this enduring element. But for this very reason fashion always stands, as I have pointed out, at the periphery of personality, which regards itself as a _pièce de résistance_ for fashion, or at least can do so when called upon.”


## __Bibliografía__

Benvenuto, Sergio. “Book Review of Georg Simmel’s _Fashion_”. _Journal of Artificial Societies and Social Simulation_, Vol. 3, núm. 2 (marzo 2000): 1-10. http://jasss.soc.surrey.ac.uk/3/2/forum/2.htm Fecha de consulta: 24 de marzo de 2020.

De Simone, Liliana. “Georg Simmel y la Moda: Hacia una comprensión de la Sociedad de Consumo en la Ciudad” En _Ciudades de George Simmel_, Francisca Márquez, ed. (Santiago: Ediciones Universidad Alberto Hurtado, 2012), 62-84.

Lipovetsky, Gilles. _El imperio de lo efímero_. Traducido por Felipe Hernández y Carmen López. Barcelona: Editorial Anagrama, 1996.


Lozano, Jorge. “Simmel: La Moda, El atractivo formal del límite”. _REIS, Revista Española de Investigaciones Sociológicas_, núm. 89, (enero-marzo 2000): 237-250.

Matteucci, Giovanni y Stefano Marino. “Introduction: Philosophical Perspectives on Fashion”. En _PHILOSOPHICAL PERSPECTIVES ON FASHION_, Giovanni Matteucci and Stefano Marino, eds. Londres y Nueva York: Bloomsbury Academic, 2017, 1-10.

Simmel, Georg. “Fashion”. _The American Journal of Sociology_, vol. 62, núm. 6 (mayo 1957): 541-558.

Svendsen, Lars. _Fashion_. Traducido por John Irons. Londres: Reaktion Books, 2006. 